main: main.o node.o file.o
	gcc main.o node.o file.o -o main

main.o: main.c
	gcc main.c -c

node.o: node.h node.c
	gcc node.c -c 

file.o: file.h file.c 
	gcc file.c -c