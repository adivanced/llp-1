import pandas as pd
import matplotlib.pyplot as plt


df_add = pd.read_csv("create.csv")
df_delete = pd.read_csv("delete.csv")

plt.plot(df_add["amount"], df_add["time"])
plt.title("Create Node")
plt.xlabel("Amount")
plt.ylabel("Time")
plt.grid(True)
plt.show()


plt.plot(df_delete["amount"], df_delete["time"])
plt.title("Delete Node")
plt.xlabel("Amount")
plt.ylabel("Time")
plt.grid(True)
plt.show()
