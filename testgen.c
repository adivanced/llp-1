#include <stdio.h>
#include <stdint.h>

FILE* ofile;

uint64_t true_rand(uint64_t start, uint64_t end){
	register uint64_t r;
	asm("rdrand %0"
		:"=r"(r)
	);

	return start + r%(end-start+1);
}


void gen_node(){
	fprintf(ofile, "%ld\n", 1);
	fprintf(ofile, "%ld\n", 0);
	uint64_t ints = true_rand(0, 10);
	fprintf(ofile, "%ld\n", ints);
	for(uint64_t i = 0; i < ints; i++){
		fprintf(ofile, "%ld\n", true_rand(0, 0xFFFFFFFE));
	}
	uint64_t floats = true_rand(0, 10);
	fprintf(ofile, "%ld\n", floats);
	for(uint64_t i  = 0; i < floats; i++){
		fprintf(ofile, "%f\n", ((float)true_rand(0, 0xFFFF)) + ((float)true_rand(0, 0xFFFF))*0.0001f);	
	}

	uint64_t strings = true_rand(0, 10);
	fprintf(ofile, "%ld\n", strings);
	for(uint64_t i = 0; i < strings; i++){
		uint64_t strlen = true_rand(1, 20);
		for(uint64_t j = 0; j < strlen; j++){
			fprintf(ofile, "%c", true_rand('a', 'z'-1));
		}
		fprintf(ofile, "%c", '\n');
	}

}

void delete_node(uint64_t i){
	fprintf(ofile, "%d\n", 3);
	fprintf(ofile, "%ld\n", i);
}


void gen_empty_node(){
	fprintf(ofile, "%d\n", 1);
	fprintf(ofile, "%d\n", 0);
	fprintf(ofile, "%d\n", 0);
	fprintf(ofile, "%d\n", 0);
	fprintf(ofile, "%d\n", 0);
}

int main(int argc, char** argv){
	ofile = fopen(argv[1], "r+");
	for(int i = 0; i < 1000; i++){
		gen_node();
		//gen_empty_node();
	}
	for(uint64_t i = 500; i >= 1; i--){
		delete_node(i);
	}
	// for(uint64_t i = 0; i < 100; i++){
	// 	gen_node();
	// }
	fprintf(ofile, "%d\n", 9999);


}